class ModsError(Exception):
    pass
class MenaceError(ModsError):
    """
    Exception to show how bad the user is.
    Most of the time reporting this error will render the issue as invalid.
    """
    pass
class V1Error(ModsError):
    """
    mod.txt related error
    """
    pass
class V2Error(ModsError):
    """
    mod.json related error
    """
    pass
class V2StructureError(V2Error):
    """
    Bad structure error/Mandatory problem
    """
    pass
