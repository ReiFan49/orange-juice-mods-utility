@echo off
ruby -e 'system("pyinstaller","-F","-n","ojmutil","--distpath","dist/pyinstaller","--key=#{sprintf(%%(%%032x),rand(1<<128)).display}","src/mods.py")'
ruby -e 'system("pyarmor","pack","-n","ojmutil","-O","dist/pyinstaller-armored","-e","-F --key=#{sprintf(%%(%%032x),rand(1<<128)).display}","src/mods.py")'
